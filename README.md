# Digicore

## 簡介
『 數碼核 』 是一款以治理為導向的微服務框架核心 ，擅長讓微服務中的狀態可視化

## 支援特性
- [ ] 熱加載 config
- [ ] 日誌
- [ ] 連線方式
   * grpc (服務端以及客戶端，參數可自由設定)
   * http1.1
   * http2
   * websocket
- [ ] 資料庫支援的有
   * orm (mysql)
   * redis
   * mongodb
- [ ] 註冊服務
- [ ] 發現服務
- [ ] corn job 任務
- [ ] queue(kafka, rocketmq)
- [ ] 異步任務池
- [ ] 事件總線（這個不清楚，再研究
- [ ] Prometheus/pprof 監控
- [ ] 優雅重啟
- [ ] 工具類 command tool
- [ ] 全域變量註冊
- [ ] 在線應用程式附載均衡
- [ ] RPC 健康檢查
- [ ] 接入授權
- [ ] ghz 壓力測試工具
- [ ] 在線服務限流
- [ ] 在線服務熔斷，異常接入 sentry
- [ ] watch 服務在線狀態
- [ ] cache 多級暫存

[//]: # ()
[//]: # (## Installation)

[//]: # ()
[//]: # (To install Gin package, you need to install Go and set your Go workspace first.)

[//]: # ()
[//]: # (1. You first need [Go]&#40;https://golang.org/&#41; installed &#40;**version 1.14+ is required**&#41;, then you can use the below Go command to install Gin.)

[//]: # ()
[//]: # (```sh)

[//]: # ($ go get -u github.com/gin-gonic/gin)

[//]: # (```)

[//]: # ()
[//]: # (2. Import it in your code:)

[//]: # ()
[//]: # (```go)

[//]: # (import "github.com/gin-gonic/gin")

[//]: # (```)

[//]: # ()
[//]: # (3. &#40;Optional&#41; Import `net/http`. This is required for example if using constants such as `http.StatusOK`.)

[//]: # ()
[//]: # (```go)

[//]: # (import "net/http")

[//]: # (```)

[//]: # ()
[//]: # (## Quick start)

[//]: # ()
[//]: # (```sh)

[//]: # (# assume the following codes in example.go file)

[//]: # ($ cat example.go)

[//]: # (```)

[//]: # ()
[//]: # (```go)

[//]: # (package main)

[//]: # ()
[//]: # (import &#40;)

[//]: # (	"net/http")

[//]: # ()
[//]: # (	"github.com/gin-gonic/gin")

[//]: # (&#41;)

[//]: # ()
[//]: # (func main&#40;&#41; {)

[//]: # (	r := gin.Default&#40;&#41;)

[//]: # (	r.GET&#40;"/ping", func&#40;c *gin.Context&#41; {)

[//]: # (		c.JSON&#40;http.StatusOK, gin.H{)

[//]: # (			"message": "pong",)

[//]: # (		}&#41;)

[//]: # (	}&#41;)

[//]: # (	r.Run&#40;&#41; // listen and serve on 0.0.0.0:8080 &#40;for windows "localhost:8080"&#41;)

[//]: # (})

[//]: # (```)

[//]: # ()
[//]: # (```)

[//]: # (# run example.go and visit 0.0.0.0:8080/ping &#40;for windows "localhost:8080/ping"&#41; on browser)

[//]: # ($ go run example.go)

[//]: # (```)

[//]: # ()
[//]: # (## Benchmarks)

[//]: # ()
[//]: # (Gin uses a custom version of [HttpRouter]&#40;https://github.com/julienschmidt/httprouter&#41;)

[//]: # ()
[//]: # ([See all benchmarks]&#40;/BENCHMARKS.md&#41;)